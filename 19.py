import os
from Crypto.Util.strxor import strxor
from aes_helpers import OPER, MODE, AES_128, COLORS

plaintexts =  [b'SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ==', \
b'Q29taW5nIHdpdGggdml2aWQgZmFjZXM=', \
b'RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ==', \
b'RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4=', \
b'SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk', \
b'T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==', \
b'T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ=', \
b'UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==', \
b'QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU=', \
b'T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl', \
b'VG8gcGxlYXNlIGEgY29tcGFuaW9u', \
b'QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA==', \
b'QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk=', \
b'QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg==', \
b'QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo=', \
b'QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=', \
b'VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA==', \
b'SW4gaWdub3JhbnQgZ29vZCB3aWxsLA==', \
b'SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA==', \
b'VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg==', \
b'V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw==', \
b'V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA==', \
b'U2hlIHJvZGUgdG8gaGFycmllcnM/', \
b'VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w=', \
b'QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4=', \
b'VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ=', \
b'V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs=', \
b'SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA==', \
b'U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA==', \
b'U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4=', \
b'VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA==', \
b'QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu', \
b'SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc=', \
b'VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs', \
b'WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs=', \
b'SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0', \
b'SW4gdGhlIGNhc3VhbCBjb21lZHk7', \
b'SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw=', \
b'VHJhbnNmb3JtZWQgdXR0ZXJseTo=', \
b'QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=' ]


aes = AES_128(os.urandom(16))
# I'm just base64 decoding these en masse so I don't have to deal with it later
plaintexts = [i.decode('base64') for i in plaintexts]
ciphertexts = [aes.ctr(i, OPER.ENCRYPT) for i in plaintexts]
keystream = ""

# Validating that the plaintext and ciphertext when xored always produce the same
# keystream and that I didn't derp in my CTR implementation, which I might have anyway
validate = [aes.fixedXOR(p, c) for p, c in zip(plaintexts, ciphertexts)]

expectedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz .,:;-'?"

# Use sorted() to get a sorted representation of engFreq and assume every plaintext char
# is the selected char from this representation. Verify each "column" of the ciphertexts 
# to see if the produced keystream generates an expectedChars value.

# Actually, I'm just going to brute force it first and see how that pans out. If it's too
# slow I'll switch to the above method.

# Just returns a column with a length check. Nothing fancy.
def getColumn(colNum):
	return [ct[colNum] for ct in ciphertexts if colNum < len(ct)]

# Brute forces a column, until it finds a value it can xor by that makes all values in
# the column equal to values within expectedChars.
def solveColumn(colNum):
	for i in range(0, 255):
		decryptedGuess = [chr(ord(c) ^ i) for c in getColumn(colNum)]
		if all(c in expectedChars for c in decryptedGuess):
			return chr(i)
	print "MISS", colNum
	return 'A'

# copied this to aes_helpers.
def decrypt(keystream):
	return [aes.fixedXOR(ct, keystream[0:len(ct)]) for ct in ciphertexts] 

for colNum in range(len(max(ciphertexts, key=len))):
	keystream += solveColumn(colNum)

COLORS.printC(COLORS.BLUE, "An attempt at decryption.")
decrypted = decrypt(keystream)
print decrypted

# manual corrections
# Since a new key is generated each time this is run, these corrections may or may not be sufficient or necessary.
final_keystream = list(keystream)
final_keystream[0] = aes.fixedXOR('I', ciphertexts[0][0])
final_keystream[1] = aes.fixedXOR(' ', ciphertexts[0][1])
final_keystream[len(ciphertexts[0]) - 1] = aes.fixedXOR('y', ciphertexts[0][len(ciphertexts[0]) - 1])
final_keystream[14] = aes.fixedXOR('m', ciphertexts[0][14])

# Now I'm just going to blow up all of the work done in the block above, because I know
# enough of the plaintext to just generate the keystream from the longest plaintext.
final_keystream = aes.fixedXOR("He, too, has been changed in his turn,", max(ciphertexts, key=len))

COLORS.printC(COLORS.BLUE, "Final Answer")
decrypted = decrypt(final_keystream)
print decrypted
