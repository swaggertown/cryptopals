from binascii import hexlify, unhexlify, b2a_base64
from Crypto.Cipher import AES
from enum import Enum
import os
import random
import pprint
import json
import urllib
import urlparse
import collections

class OPER(Enum):
	ENCRYPT = 0
	DECRYPT = 1

class MODE(Enum):
	AES_ECB = 0
	AES_CBC = 1

class COLORS:
	FAIL = '\033[31m'
	SUCCESS = '\033[32m'
	BLUE = '\033[34m'
	END = '\033[0m'

def printAll(string):
	print string.encode('string_escape')

def printC(code, string):
	print code + string + COLORS.END

def fixedXOR(a, b):
	return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b))

def pkcs7(inp, bs=16):
	pad = (bs - (len(inp) % bs)) if len(inp) % 16 != 0 else 16 # % bs
	return inp + "".join(chr(pad))*pad

def pkcs7_strip(inp, bs=16):
	last_char = inp[-1]
	if inp[-ord(last_char)] == last_char and len(inp) % 16 == 0:
		return inp[:-ord(last_char)] 
	else:
		raise ValueError

def aes_128_ecb(text, oper, key="YELLOW SUBMARINE", enablePadding=True, padding=pkcs7):
	cipher = AES.AESCipher(key, AES.MODE_ECB)
	if oper == OPER.DECRYPT:
		if enablePadding:
			return pkcs7_strip(cipher.decrypt(text))
		else:
			return cipher.decrypt(text)
	else:
		if enablePadding:
			return cipher.encrypt(padding(text))
		else:
			return cipher.encrypt(text)

# This is messy and could be made much cleaner, but it works
def aes_128_cbc(text, oper, key="YELLOW SUBMARINE", iv=os.urandom(16), padding=pkcs7):
	output = ""
	xorin = iv
	prevCT = iv
	if oper == OPER.ENCRYPT:
		text = padding(text)
	for i in range(0, len(text), 16):
		block = text[i:i+len(key)]
		if oper == OPER.ENCRYPT:
			xorin = aes_128_ecb(fixedXOR(xorin, block), oper, key, False)
		else:
			xorin = fixedXOR(prevCT, aes_128_ecb(block, oper, key, False))
			prevCT = block
		output += xorin
	if oper == OPER.DECRYPT:
		output = pkcs7_strip(output)
	return output

print aes_128_cbc(aes_128_cbc("Hello.", OPER.ENCRYPT), OPER.DECRYPT)

def encryption_oracle(encrypt, garbagepad=False, pt="A"*64, bs=16):
	if garbagepad:
		prepend_count, append_count = random.randint(5, 10), random.randint(5, 10)
		pt = os.urandom(prepend_count) + pt + os.urandom(append_count)
	ct = encrypt(pt, OPER.ENCRYPT, os.urandom(bs))
	blocks = [ct[i:i+bs] for i in range(0, len(ct), bs)]
	# print COLORS.BLUE + "Blocks:", len(blocks), "Set:", len(set(blocks)), COLORS.END
	# pprint.pprint(blocks)
	if len(blocks) != len(set(blocks)):
		return MODE.AES_ECB
	else:
		return MODE.AES_CBC
	
def test_encryption_oracle():
	mode = MODE(random.randint(0, 1))
	if mode == MODE.AES_ECB:
		result = encryption_oracle(aes_128_ecb, True)
	else:
		result = encryption_oracle(aes_128_cbc, True)

	if mode == result:
		printC(COLORS.SUCCESS, "Correctly Guessed " + str(mode))
	else:
		printC(COLORS.FAIL, "Should Have Guessed " + str(mode))

def prefix_encrypt(prefix, random=False):
	global globalkey
	suffix = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".decode('base64')
	if random:
		prefix = os.urandom(random.randint(0, 100))
	return aes_128_ecb(prefix + suffix, globalkey)

def find_key_length(encrypt):
	global globalkey
	curLen, initLen, numbytes = 0, 0, 0
	while curLen == initLen:
		curLen = len(encrypt("A"*numbytes))
		initLen = curLen if (initLen == 0) else initLen
		numbytes += 1
	# The +2 below is to account for the post-compute update of numbytes and that numbytes will be 
	# the number of bytes required to bump the length up a block, meaning even without the post-compute 
	# update it'd still be off by one.
	# Not sure the comment above is correct?
	# Seriously, good luck figuring this shit out, future me, because present me is fucking lost.
	return curLen - initLen, curLen - (curLen - initLen) - numbytes + 2

def confirm_ecb(encrypt):
	global globalkey
	bs = find_key_length(encrypt)[0]
	ct = encrypt("A"*(3*bs))
	blocks = [ct[i:i+bs] for i in range(0, len(ct), bs)]
	return True if (len(blocks) != len(set(blocks))) else False

# And here I thought the find_key_length function was a bit confusing...
# This code should probably be killed with fire.
def simple_baat_decrypt(encrypt):
	global globalkey
	bs = find_key_length(encrypt)[0]
	nb = find_key_length(encrypt)[1]
	assert confirm_ecb(encrypt)

	dcblocks = ""
	curB = 1
	for i in range(0, nb, bs):
		# Each block
		ptblock = ""
		for j in range(1, bs + 1):
			inp = "A"*(bs-j) if i==0 else dcblocks[curB:curB+(bs-1)]
			ct = encrypt(inp)
			block = ct[i:i+bs]
			d = {}
			for k in range(0, 255):
				d[encrypt(inp + ptblock + chr(k))[0:bs]] = inp + chr(k)
			if block in d:
				ptblock += d[block][bs-j]
			else:
				break
			if i != 0:
				curB += 1

		dcblocks += ptblock
	print dcblocks

def find_input_location(encrypt):
	global globalkey
	bs = find_key_length(encrypt)[0]
	rep_len = 3*bs-1
	input_str = 'A'*rep_len
	output = encrypt(input_str)
	blocks = [output[i:i+bs] for i in range(0, len(output), bs)]
	# duploc = blocks.index([item for item, count in collections.Counter(blocks).items() if count > 1][0])
	if blocks[0] == blocks[1]:
		return 0
	while len(blocks) != len(set(blocks)):
		input_str = 'B'*(bs - rep_len) + 'A'*rep_len
		rep_len -= 1
		output = encrypt(input_str)
		blocks = [output[i:i+bs] for i in range(0, len(output), bs)]
	return 3*bs-1 - rep_len - 1

def harder_baat_decrypt(encrypt):
	global globalkey
	bs = find_key_length(encrypt)[0]
	nb = find_key_length(encrypt)[1]
	assert confirm_ecb(encrypt)

	begin = find_input_location(encrypt)
	always_prepend = 'P'*(bs - begin % bs) if bs!=0 else ""
	fcb = (begin/bs + 1)*bs			# first complete user controlled block

	dcblocks = ""
	curB = 1
	for i in range(fcb, nb + 1*bs, bs):
		# Each block
		ptblock = ""
		for j in range(1, bs + 1):
			# print "(i, j)", i, j
			inp = always_prepend
			inp += "A"*(bs-j) if i==(begin/bs+1)*bs else dcblocks[curB:curB+(bs-1)]
			# print inp
			ct = encrypt(inp)
			block = ct[i:i+bs]
			d = {}
			for k in range(0, 255):
				d[encrypt(inp + ptblock + chr(k))[fcb:fcb+bs]] = inp.strip(always_prepend) + chr(k)
			if block in d:
				ptblock += d[block][bs-j]
			else:
				break
			if i != fcb:
				curB += 1

		dcblocks += ptblock
	return pkcs7_strip(dcblocks)

def parse_url(cookie):
	blocks = [cookie[i:i+16] for i in range(0, len(cookie), 16)]
	print blocks
	return urlparse.parse_qs(cookie)

# This is sooper janky
def profile_for(email):
	aslist = [
		['email', email.replace('&', '').replace('=', '')],
		['uid', 10],
		['role', 'user']
	]
	asstring = ""
	for k in aslist:
		asstring += k[0] + "=" + str(k[1]) + "&"
	return asstring.rstrip('&')

def encrypted_cookies(string):
	global globalkey
	return aes_128_ecb(string, OPER.ENCRYPT, globalkey)

def cbc_bitflip_encrypt(string):
	global globalkey
	return aes_128_cbc("comment1=cooking%20MCs;userdata=" + string.replace('=', '').replace('&', '') + ";comment2=%20like%20a%20pound%20of%20bacon", OPER.ENCRYPT, '\x00'*16, globalkey)

def cbc_bitflip_parse(string):
	global globalkey
	pt = aes_128_cbc(string, OPER.DECRYPT, '\x00'*16, globalkey)
	print [pt[i:i+16] for i in range(0, len(pt), 16)]
	return [s.split("=") for s in pt.split(";")]

# print pkcs7("YELLOW SUBMARINE", bs=20)
# with open("10.txt") as f:
# 	content = f.read().decode('base64')
# print aes_128_cbc(content, OPER.DECRYPT)
# aes_128_ecb("testing", OPER.ENCRYPT)
# print aes_128_cbc(aes_128_cbc("testingtestingtestingtestingtestingtestingtestingtestingtestingtestingtestingtestingtesting", OPER.ENCRYPT), OPER.DECRYPT)
# seed = random.SystemRandom().randint(0, 2**32-1)
# for i in range(0, 300):
# 	rand_value = os.urandom(32)
# 	# test_encryption_oracle(os.urandom(16) + rand_value + os.urandom(16) + rand_value + os.urandom(16))
# 	# test_encryption_oracle("89ABCDEF0123456789ABCDEF0123456789ABCDEF01234567")
# 	test_encryption_oracle()

# Simple ECB BAAT
# globalkey = os.urandom(16)
# simple_baat_decrypt(prefix_encrypt)

# ECB Cut and Paste
# globalkey = os.urandom(16)
# email1 = encrypted_cookies(profile_for("foo@bar.coadmin" + '\x0b'*11))
# email2 = encrypted_cookies(profile_for("foo@bar.commm"))
# print parse_url(aes_128_ecb(email1, OPER.DECRYPT, globalkey))
# print parse_url(aes_128_ecb(email2, OPER.DECRYPT, globalkey))
# print parse_url(aes_128_ecb(email2[0:32] + email1[16:32], OPER.DECRYPT, globalkey))

# Harder ECB BAAT
# globalkey = os.urandom(16)
# print harder_baat_decrypt(prefix_encrypt)
# printC(COLORS.BLUE, "Done.")

# CBC Bitflip Attacks
globalkey = os.urandom(16)
ct = cbc_bitflip_encrypt('x'*16 + "admin?true")
print format(int('='.encode('hex'), 16), '07b')
print chr(int("0111111", 2))
print [ct[i:i+16].encode('hex') for i in range(0, len(ct), 16)]
modified_ct = ct[0:37] + chr(ord(ct[37]) ^ 2) + ct[38:len(ct)]
# print ct.encode('hex')
cbc_bitflip_parse(modified_ct)
