from binascii import hexlify, unhexlify, b2a_base64
from collections import Counter
from Crypto.Cipher import AES

relFactor = 9.0/10
engFreq = { 'A': 8.167,
'B': 1.492,
'C': 2.782,
'D': 4.253,
'E': 12.702,
'F': 2.228,
'G': 2.015,
'H': 6.094,
'I': 6.966,
'J': 0.153,
'K': 0.772,
'L': 4.025,
'M': 2.406,
'N': 6.749,
'O': 7.507,
'P': 1.929,
'Q': 0.095,
'R': 5.987,
'S': 6.327,
'T': 9.056,
'U': 2.758,
'V': 0.978,
'W': 2.360,
'X': 0.150,
'Y': 1.974,
'Z': 0.074,
' ': 15.00 }

for k in engFreq:
	engFreq[k] = engFreq[k] * relFactor / 100.0

def scorePlaintextCandidate(p):
	score = 0
	alphCount = 0
	tolerance = 30.0 / 100		# percentage
	candidate = "".join(p).upper()
	canFreq = Counter(candidate)
	expFreq = {k: v*len(candidate) for k, v in engFreq.items()}
	difFreq = {k: canFreq[k] - v for k, v in expFreq.items()}
	# print candidate
	# print "Expected:", expFreq
	# print "Actual:", canFreq
	# print "Differentials:",difFreq
	for k, v in difFreq.items():
		score += v
		alphCount += canFreq[k]
	# print "Alphabeticals:", alphCount
	score = (alphCount - abs(score)) / len(candidate)
	# print score
	return score

def hextob64(h):
	return b2a_base64(unhexlify(h)).rstrip("\n")

# Accepts shit as hex
def fixedXOR(xa, xb):
	a, b = xa.decode("hex"), xb.decode("hex")
	return hexlify("".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b)))

# Accepts ASCII char and returns 7 bits of binary, with leading 0s
def c2b(ch):
	return format(int(ch), '07b')

# Accepts ASCII
def hamming(xa, xb):
	a, b = ''.join(map(c2b, bytearray(xa))), ''.join(map(c2b, bytearray(xb)))
	# print "A:", a
	# print "B:", b
	assert len(a) == len(b)
	dist = 0
	# This should be made a list comprehension later.
	for i, j in zip(a, b):
		if i != j:
			dist += 1
	return dist

def singleXOR(xc, xa):
	a, c = xa.strip().decode("hex"), xc.decode("hex")
	# print xa, xc
	return hexlify("".join(chr(ord(x) ^ ord(c)) for x in a))

def decryptSingleXOR(ct):
	p = []
	scores = []
	for i in xrange(0, 256):
		string = singleXOR(chr(i).encode("hex"), ct.encode("hex"))
		p.append(string.decode("hex"))
		scores.append(scorePlaintextCandidate(p[i]))
	# print p
	
	# find max
	maxIndex = 0
	maxValue = scores[0]
	for i in xrange(0, 256):
		if scores[i] > maxValue:
			maxIndex = i
			maxValue = scores[i]
	
	return (p[maxIndex], scores[maxIndex], chr(maxIndex))

def problem4():
	with open("4.txt") as f:
		content = f.readlines()
	content = [x.strip() for x in content]
	candidates = [decryptSingleXOR(c) for c in content]
	print "Best Guess:", max(candidates, key=lambda i:i[1])[0]

def vigenereEncrypt(p, k):
	key = "".join(k[i % len(k)] for i,c in enumerate(p))
	# print key
	return fixedXOR(p.encode("hex"), key.encode("hex"))

def vigenereCrack(ct, ks):
	print "vigenereCrack", ks
	blocks = []
	for k in xrange(0, ks):
		blocks.append("".join(ct[i] for i, j in enumerate(ct) if i % ks == k))
	
	# This could really be combined with the above loop to make it cleaner, but left this way rn for possible debugging
	blocksSolved = []
	key = []
	for b in blocks:
		sXORResult = decryptSingleXOR(b)
		sXOR = sXORResult[0]
		key.append(sXORResult[2])
		if len(b) != len(blocks[0]):
			sXOR = sXOR + "\0"
		blocksSolved.append(sXOR)

	# # NOTE Reverse the transpose here.
	# solution = ""
	# for i in range(0, len(blocksSolved[0])):
	# 	for b in blocksSolved:
	# 		solution += b[i]

	# print "Blocks:", blocks
	# print "BlocksSolved:", blocksSolved
	# print "Solution:", solution
	print "KEY:", key
	return "".join([b[i] for i in range(0, len(blocksSolved[0])) for b in blocksSolved])

# Returns a sorted list with most probable key len at position 0
def vigenereKeySizeGuess(ct):
	results = []
	for i in xrange(2, max(2, len(ct)/4)):
		blocks = [ct[k*i:(k+1)*i] for k in range(0, 4)]
		ndists = [hamming(blocks[k - 1], blocks[k])/float(i) for k in range(0, 4)]
		ndist = hamming(blocks[0], blocks[1]) / float(i)
		# results.append((i, ndist))
		results.append((i, sum(ndists)/(float(4))))
	results.sort(key=lambda tup: tup[1], reverse=False)
	return results
			
def problem6():
	with open("6.txt") as f:
		content = f.read()
	content = "".join([x.strip('\n') for x in content]).decode('base64')
	keySizes = vigenereKeySizeGuess(content)[0:15]
	# keySizes.append((29, 0))
	solutions = []
	for ks in keySizes:
		s = vigenereCrack(content, ks[0])
		solutions.append(s)
		print str(ks) + ":", s

def problem7():
	key = b'YELLOW SUBMARINE'
	cipher = AES.AESCipher(key, AES.MODE_ECB)
	with open("7.txt") as f:
		content = f.read().decode('base64')
	pt = cipher.decrypt(content)
	print "Decrypted:", pt

def problem8(bs = 16):
	ecb_candidates = []
	with open("8.txt") as f:
		content = f.read().splitlines()
	for l in content:
		blocks = [l[i:i+bs] for i in range(0, len(l), bs)]
		if len(blocks) != len(set(blocks)):
			ecb_candidates.append(l)
	print ecb_candidates
	
# print hextob64("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")
# print "XOR:", fixedXOR("1c0111001f010100061a024b53535009181c", "686974207468652062756c6c277320657965")
# print "singleXOR:", decryptSingleXOR("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")[0]
# problem4()
# vEncrypted = vigenereEncrypt("Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal", "ICE")
# print vEncrypted
# hdist = hamming("this is a test", "wokka wokka!!!")
# print "Hamming Distance:", hdist
# print vigenereCrack("0123456789AB", 4)
# problem6()
# problem7()
problem8()
