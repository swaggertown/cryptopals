from Crypto.Cipher import AES
from enum import Enum
import os
import random
import pprint

class OPER(Enum):
	ENCRYPT = 0
	DECRYPT = 1

class MODE(Enum):
	AES_ECB = 0
	AES_CBC = 1

class colors:
	FAIL = '\033[91m'
	SUCCESS = '\033[92m'
	BLUE = '\033[94m'
	END = '\033[0m'

def printAll(string):
	print string.encode('string_escape')

def printC(code, string):
	print code + string + colors.END

def fixedXOR(a, b):
	return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b))

def pkcs7(inp, bs=16):
	pad = (bs - (len(inp) % bs)) % bs
	return inp + "".join(chr(pad))*pad

def aes_128_ecb(text, oper, key="YELLOW SUBMARINE", padding=pkcs7):
	cipher = AES.AESCipher(key, AES.MODE_ECB)
	if oper == OPER.DECRYPT:
		return cipher.decrypt(text)
	else:
		return cipher.encrypt(padding(text))

# This is messy and could be made much cleaner, but it works
def aes_128_cbc(text, oper, key="YELLOW SUBMARINE", iv=os.urandom(16), padding=pkcs7):
	output = ""
	xorin = iv
	prevCT = iv
	text = padding(text)
	for i in range(0, len(text), 16):
		block = text[i:i+len(key)]
		if oper == OPER.ENCRYPT:
			xorin = aes_128_ecb(fixedXOR(xorin, block), oper, key)
		else:
			xorin = fixedXOR(prevCT, aes_128_ecb(block, oper, key))
			prevCT = block
		output += xorin
	return output

def encryption_oracle(pt, encrypt, bs=16):
	
	ct = encrypt(pt, OPER.ENCRYPT, os.urandom(bs))
	blocks = [ct[i:i+bs] for i in range(0, len(ct), bs)]
	# print colors.BLUE + "Blocks:", len(blocks), "Set:", len(set(blocks)), colors.END
	# pprint.pprint(blocks)
	if len(blocks) != len(set(blocks)):
		return MODE.AES_ECB
	else:
		return MODE.AES_CBC
	
def test_encryption_oracle(pt):
	key = os.urandom(16)
	mode = MODE(random.randint(0, 1))
	prepend_count, append_count = random.randint(5, 10), random.randint(5, 10)
	pt = os.urandom(prepend_count) + pt + os.urandom(append_count)
	# printAll(pt)
	if mode == MODE.AES_ECB:
		ct = (aes_128_ecb(pt, OPER.ENCRYPT, key), MODE.AES_ECB)
	else:
		iv = os.urandom(16)
		ct = (aes_128_cbc(pt, OPER.ENCRYPT, key, iv), MODE.AES_CBC)
		# print "Plaintext:", pt.encode('string_escape')
		# print "Decrypted:", aes_128_cbc(ct[0], OPER.DECRYPT, key, iv).encode('string_escape')

	if encryption_oracle(ct[0]) == ct[1]:
		printC(colors.SUCCESS, "Correctly Guessed " + str(ct[1]))
	else:
		printC(colors.FAIL, "Should Have Guessed " + str(ct[1]))

def byte_at_a_time_decrypt(prefix):
	key = os.urandom(16)
	suffix = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".decode('base64')

	
# print pkcs7("YELLOW SUBMARINE", bs=20)
# with open("10.txt") as f:
# 	content = f.read().decode('base64')
# print aes_128_cbc(content, OPER.DECRYPT)
# aes_128_ecb("testing", OPER.ENCRYPT)
# print aes_128_cbc(aes_128_cbc("testingtestingtestingtestingtestingtestingtestingtestingtestingtestingtestingtestingtesting", OPER.ENCRYPT), OPER.DECRYPT)
seed = random.SystemRandom().randint(0, 2**32-1)
for i in range(0, 300):
	rand_value = os.urandom(32)
	# test_encryption_oracle(os.urandom(16) + rand_value + os.urandom(16) + rand_value + os.urandom(16))
	# test_encryption_oracle("89ABCDEF0123456789ABCDEF0123456789ABCDEF01234567")
	test_encryption_oracle("A"*64)
