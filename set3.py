import os
import random
from aes_helpers import OPER, MODE, COLORS, AES_128

aes = AES_128(os.urandom(16))

def randomSelect():
	l = ["MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=", "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=", "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==", "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==", "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl", "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==", "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==", "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=", "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=", "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"]
	# for a in l:
	# 	print a.decode('base64')
	plaintext = l[random.randint(0, 9)].decode('base64')
	print plaintext, len(plaintext)
	return aes.cbc(plaintext, OPER.ENCRYPT)

def checkPadding(ct):
	try:
		pt = aes.cbc(ct, OPER.DECRYPT)
	except ValueError:
		return False
	return True

ct = randomSelect()

def decryptBlock(block, prev_block):
	print block.encode('hex'), "\n", prev_block.encode('hex')
	pt_block = ""
	# Iterate through block from last position to first
	for block_pos in range(15, -1, -1):
		# NAO FOR SOME BRUTE FORCING
		print "Block Position:", block_pos
		pprime_padding = chr(16 - block_pos)
		for c in range(0, 255):
			cprime = '\x00'*block_pos + chr(c) + block
			if checkPadding(cprime):
				print pprime_padding.encode('hex'), prev_block[block_pos].encode('hex'), chr(c).encode('hex')
				pt_block_char = chr(ord(pprime_padding) ^ ord(prev_block[block_pos]) ^ c)
				pt_block = pt_block_char + pt_block
				print pt_block
				print pt_block.encode('hex')

				# prepare for next char
				print "Invoking loop", 15 - block_pos + 1
				block = block[-16:]
				prepend = ""
				for i in range(1, 15 - block_pos + 2):
					print block_pos + i - 1
					prepend = chr((ord(pprime_padding) + i) ^ ord(pt_block_char) ^ ord(prev_block[block_pos + i - 1])) + prepend
				block = prepend + block

# Divide in to blocks
blocks = [ct[i:i+16] for i in range(0, len(ct), 16)]
for pos in range(1, len(blocks)):
	decryptBlock(blocks[pos], blocks[pos - 1])
