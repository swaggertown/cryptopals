from binascii import hexlify, unhexlify, b2a_base64
from Crypto.Cipher import AES
from enum import Enum
import os
import random
import pprint
import json
import urllib
import urlparse
import collections

class OPER(Enum):
	ENCRYPT = 0
	DECRYPT = 1

class MODE(Enum):
	AES_ECB = 0
	AES_CBC = 1

class COLORS:
	FAIL = '\033[31m'
	SUCCESS = '\033[32m'
	BLUE = '\033[34m'
	END = '\033[0m'

	@staticmethod
	def printC(code, string):
		print code + string + COLORS.END

class AES_128:
	def __init__(self, key=os.urandom(16), iv=os.urandom(16)):
		self.key = key
		self.iv = iv

	@staticmethod
	def fixedXOR(a, b):
		return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b))

	@staticmethod
	def ctr_decrypt_keystream(ct, keystream):
		return AES_128.fixedXOR(ct, keystream[0:len(ct)])

	def pkcs7(self, inp, bs=16):
		pad = (bs - (len(inp) % bs)) if len(inp) % 16 != 0 else 16 # % bs
		return inp + "".join(chr(pad))*pad

	def pkcs7_strip(self, inp, bs=16):
		last_char = inp[-1]
		try:
			if inp[-ord(last_char)] == last_char and len(inp) % 16 == 0:
				return inp[:-ord(last_char)] 
			else:
				raise ValueError
		except IndexError:
			raise ValueError

	def ecb(self, text, oper, enablePadding=True, padding=pkcs7):
		cipher = AES.AESCipher(self.key, AES.MODE_ECB)
		if oper == OPER.DECRYPT:
			if enablePadding:
				return self.pkcs7_strip(cipher.decrypt(text))
			else:
				return cipher.decrypt(text)
		else:
			if enablePadding:
				return cipher.encrypt(padding(self, text))
			else:
				return cipher.encrypt(text)

	# This is messy and could be made much cleaner, but it works
	def cbc(self, text, oper, iv=os.urandom(16), padding=pkcs7):
		output = ""
		xorin = iv
		prevCT = iv
		if oper == OPER.ENCRYPT:
			text = padding(self, text)
		for i in range(0, len(text), 16):
			block = text[i:i+len(self.key)]
			if oper == OPER.ENCRYPT:
				xorin = self.ecb(self.fixedXOR(xorin, block), oper, False)
			else:
				xorin = self.fixedXOR(prevCT, self.ecb(block, oper, False))
				prevCT = block
			output += xorin
		if oper == OPER.DECRYPT:
			output = self.pkcs7_strip(output)
		return output
	
	# This can only handle nonces that will fit in one byte.
	def ctr(self, text, oper, initLoc=8, nonce=0, bs=16):
		keystream = ""
		for i in range(0, len(text) / 16 + (len(text) % 16 > 0)):
			keystream += ('\x00' * (bs - initLoc)) + chr(nonce) + ('\x00' * (bs - initLoc - 1))
			nonce += 1
			# print keystream.encode('string_escape')
		keystream = self.ecb(keystream, OPER.ENCRYPT, False)
		print "Keystream Used", keystream.encode('hex')
		return self.fixedXOR(keystream[0:len(text)], text)
