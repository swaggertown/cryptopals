import os
from aes_helpers import AES_128, OPER, MODE
import set1

with open('20.txt') as f:
	plaintexts = [l.decode('base64') for l in f.read().splitlines()]

aes = AES_128()
ciphertexts = [aes.ctr(i, OPER.ENCRYPT) for i in plaintexts] 

# truncate ciphertexts to the length of the shortest ciphertext
ciphertexts = [c[0:len(min(ciphertexts, key=len))] for c in ciphertexts]

ct_blob = ''.join(ciphertexts)
keystream = set1.vigenereCrack(ct_blob, len(ciphertexts[0]))
print keystream
decrypted = [aes.ctr_decrypt_keystream(c, keystream) for c in ciphertexts]
# print decrypted

# the vigenereCrack function is silly and returns the decrypted text, not the key
# I'll probably modify this in the future, if I get to it to make it cleaner.

