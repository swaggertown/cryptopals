# 32 bit constants
# Copy/pasted constants from ricpacca's GitHub.

W, N, M, R = 32, 624, 397, 31
A = 0x9908B0DF
U, D = 11, 0xFFFFFFFF
S, B = 7, 0x9D2C5680
T, C = 15, 0xEFC60000
L = 18
F = 1812433253
LOWER_MASK = (1 << R) - 1

class MT19937:
	def __init__(self, seed):
		self.MT = [0]*n
		self.MT[0] = seed
		self.lower_mask = (1 << r) - 1
		# self.upper_mask = 
